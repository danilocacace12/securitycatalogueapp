<div class="container-fluid" ng-init="ctrl.disabledAll()">

  <!-- As a heading -->
  <div class="container">
    <nav class="navbar navbar-default">
      <span class="navbar-brand mb-0 h1" style="text-align:center;">Security Catalog</span>
    </nav>
  </div>

<div class="row">


  <!-- Colonna con immagine -->
	 <div class="col-lg-6">
     	<img src="images/img2.png" class="img-fluid" alt="picture1">
   </div>

   <!-- Colonna con form e bottoni -->
   <div class="col-lg-6" style="border: 1px solid #a4a4a4; margin-top:15px; margin-bottom:15px; border-radius: 4px; padding: 1px;">
   
     <p class="h6" style="text-align:center; margin-top: 5px;">Seleziona concetto</p>
     <!-- Riga form e button-->
     <div class="row" style="margin-top: 10px;">
       
       <!-- Colonna con form -->
       <div class="col-10">
         <select class="form-control" id="form_concetto" >
         <option>Asset</option>
         <option>Countermeasure</option>
         <option>Threat</option>
         <option>Weakness</option>
         </select>
       </div>
        <!-- Colonna con bottone -->
       <div class="col-2">
         <button type="button" ng-click="ctrl.load_query_description()" class="btn btn-success" id="button_form_concetto" style="padding-left:22px; padding-right:22px;">OK</button>
       </div>
     </div>

     <p class="h6" style="text-align:center; margin-top: 15px;">Analisi da effettuare</p>
     <!-- Riga form e button-->
     <div class="row" style="margin-top: 10px;">
       <!-- Colonna con form -->
       <div class="col-10">

         <select class="form-control" id="form_analysis" ></select>
       </div>
        <!-- Colonna con bottone -->
       <div class="col-2">
         <button type="button" ng-click="ctrl.load_parameters()" class="btn btn-success" id="button_form_analysis" style="padding-left:22px; padding-right:22px;">OK</button>
       </div>
     </div>

     <p class="h6" id="text_parameter" style="text-align:center; margin-top: 15px;">Seleziona parametro</p>
     <!-- Riga form e button-->
     <div class="row" style="margin-top: 10px;">
       <!-- Colonna con form -->
       <div class="col-10">

         <select class="form-control" id="form_parameters"></select>
       </div>
        <!-- Colonna con bottone -->
       <div class="col-2">
         <button type="button" ng-click="ctrl.load_response()" class="btn btn-success" id="button_form_parameters" style="padding-left:22px; padding-right:22px;">OK</button>
       </div>
     </div>

     <p class="h6" style="text-align:center; margin-top: 15px;">Risultati</p>
     <!-- Riga form e button-->
     <div class="row" style="margin-top: 10px;">
       <!-- Colonna con form -->
       <div class="col-10">

         <select class="form-control" id="form_results"></select>
       </div>
        <!-- Colonna con bottone -->
       <div class="col-2">
         	<button type="button" ng-click="ctrl.change_description()" class="btn btn-success" id="button_form_results" style="padding-left:22px; padding-right:22px;">OK</button>
       </div>
     </div>

     <!-- Riga textarea-->
         <div class="row" style="margin-top: 15px;">
         	<!-- Colonna con textarea -->
            <div class="col-10">
              <textarea class="form-control" style="margin-bottom:10px" id="description_area" rows="5"></textarea>
           </div>
           <!-- Colonna con bottone -->
           <div class="col-2">
           	<button type="button" ng-click="ctrl.reset_all()" class="btn btn-success" id="button_reset" style="margin-top:65%;">RESET</button>
           </div>
         </div>
   </div>
   
</div>

</div>
