var app = angular.module('crudApp',['ui.router','ngStorage']);

app.constant('urls', {
    BASE: 'http://localhost:8080/SpringBootApp',
    SERVICE_API: 'http://localhost:8080/SpringBootApp/api/'
});


app.config(['$stateProvider', '$urlRouterProvider', 
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/home',
                controller:'AppController',
                controllerAs:'ctrl',
              

            });
            $urlRouterProvider.otherwise('/');
        
    }]);

