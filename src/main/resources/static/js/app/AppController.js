'use strict';

angular.module('crudApp').controller('AppController',
	['AppService', '$scope' ,  function( AppService, $scope) {
	    	
			var self = this;	
			var description=[];
		
			self.disabledAll = disabledAll;
			self.load_query_description = load_query_description;
			self.load_parameters = load_parameters;
			self.load_response = load_response;
			self.change_description = change_description;
			self.reset_all = reset_all;
			self.removeOptions =removeOptions;

			function disabledAll(){
				button_form_analysis.disabled = true;
				form_analysis.disabled = true;
				
				button_form_parameters.disabled = true;
				form_parameters.disabled = true;
				
				form_results.disabled = true;
				button_form_results.disabled = true;
				description_area.readOnly = true;
			}
			
			function load_query_description(){
				AppService.loadQueryDescription(form_concetto.value)
				.then(
						function(response){
							button_form_analysis.disabled=false;
							form_analysis.disabled=false;
							
							button_form_concetto.disabled=true;
							form_concetto.disabled=true;
							
							document.getElementById("text_parameter").innerHTML="Seleziona "+form_concetto.value;
							var i;
							for(i=0; i<response.length; i++) {
								var option = document.createElement("option");
								option.text = response[i];
								form_analysis.add(option);
							}
						},
						function(errResponse){
							console.error("Non è  stato possibile caricare la descrizione delle query");
						}
				);
			}
			
			function load_parameters(){
				AppService.loadParameters(form_concetto.value)
				.then(
					    function(response){
					    	button_form_analysis.disabled = true;
							form_analysis.disabled = true;
							
							button_form_parameters.disabled = false;
							form_parameters.disabled = false;
							
							var i;
							
							for(i=0; i<response.length; i++) {
								var option = document.createElement("option");
								option.text = response[i];
								form_parameters.add(option);
							}
						},
						function(errResponse){
							console.error("Non è  stato possibile caricare i parametri della richiesta");
						}
				);
			}
			
			function load_response(){
				AppService.loadResponse(form_concetto.value, form_analysis.selectedIndex, form_parameters.selectedIndex)
				.then(
						function(response){
							button_form_parameters.disabled = true;
							form_parameters.disabled = true;
							
							form_results.disabled = false;
							button_form_results.disabled = false;
							description_area.style.backgroundColor = "#ffffff";
							
							//Ciclo if nel caso sono component_types
							if(response[0].id==1){
								var i;
								for(i=1; i<response.length; i++) {
									description.push(response[i].description);
									var option = document.createElement("option");
									option.text = response[i].name;
									form_results.add(option);
								}
							}
							
							//Ciclo if nel caso di Controls
							if(response[0].id==2){
								var i;
								for(i=1; i<response.length; i++) {
									description.push(response[i].control_description);
									var option = document.createElement("option");
									option.text = response[i].control_id + ": "+ response[i].control_name;
									form_results.add(option);
								}
							}
							
							//Ciclo if nel caso di Questions_threat
							if(response[0].id==3){
								var i;
								for(i=1; i<response.length; i++) {
									description.push(response[i].description);
									var option = document.createElement("option");
									option.text = response[i].question_id;
									form_results.add(option);
								}
							}
							
							//Ciclo if nel caso di Threat
							if(response[0].id==4){
								var i;
								for(i=1; i<response.length; i++) {
									description.push(response[i].threat_description);
									var option = document.createElement("option");
									option.text = response[i].threat_name;
									form_results.add(option);
								}
							}
							description_area.value = description[0];
						},
						function(errResponse){
							console.error("Non è  stato possibile caricare le risposte alla richiesta");
						}
				);
			}
			
			function change_description(){
				description_area.value = description[form_results.selectedIndex];
			}
			
			function removeOptions(selectbox)
			{
			    var i;
			    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
			    {
			        selectbox.remove(i);
			    }
			}
			
			function reset_all(){
				button_form_concetto.disabled=false;
				form_concetto.disabled=false;
				disabledAll();
				description_area.value = "";
				var i;
				var g = form_results.length;
				removeOptions(form_results);
				removeOptions(form_parameters);
				removeOptions(form_analysis);
				document.getElementById("text_parameter").innerHTML="Seleziona parametro";
				description_area.style.backgroundColor = "#e9ecef";
				
			}
	}
	
	]);