'use strict';

angular.module('crudApp').factory('AppService',
		['$localStorage', '$http', '$q', 'urls',
	        function ($localStorage, $http, $q, urls) {
	    		var factory = {
	    			loadAllThreats: loadAllThreats,
	    			getAllThreats: getAllThreats,
	    			loadAllComponents_types: loadAllComponents_types,
	    			getAllComponents_types: getAllComponents_types,
	    			loadAllQuestions_threats: loadAllQuestions_threats,
	    			getAllQuestions_threats: getAllQuestions_threats,
	    			
	    			//Da cancellare se non usati
	    			loadThreats_description: loadThreats_description,
	    			
	    			loadQuestion_description: loadQuestion_description,
	    			
	    			loadComponent_description: loadComponent_description,
	    			//________--_____--____
	    			
	    			loadQueryDescription: loadQueryDescription,
	    			loadParameters: loadParameters,
	    			loadResponse: loadResponse,
	    		};
	    		return factory;
	    		
	    		
	    		
	    		function loadAllThreats(){
	    			var deferred = $q.defer();
	    			$http.get(urls.SERVICE_API+'threat')
	    				.then(
	    					function(response){
	    						console.log('Caricati Tutti i Threat con successo');
	    						$localStorage.threats = response.data;
	    						deferred.resolve(response);
	    					},
	    					function(errResponse) {
	    						console.error('Errore nel caricamento dei Threat');
	    						deferred.reject(errResponse);
	    					}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function getAllThreats(){
	    			return $localStorage.threats;
	    		}
	    		
	    		function loadAllComponents_types(){
	    			var deferred = $q.defer();
	    			$http.get(urls.SERVICE_API+'component_types')
	    				.then(
	    					function(response){
	    						console.log('Caricati Tutti i Component_types con successo');
	    						$localStorage.components_types = response.data;
	    						deferred.resolve(response);
	    					},
	    					function(errResponse) {
	    						console.error('Errore nel caricamento dei Component Types');
	    						deferred.reject(errResponse);
	    					}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function getAllComponents_types(){
	    			return $localStorage.components_types;
	    		}
	    		
	    		function loadAllQuestions_threats(){
	    			var deferred = $q.defer();
	    			$http.get(urls.SERVICE_API+'questions_threat')
	    				.then(
	    					function(response){
	    						console.log('Caricati Tutti i Questions Threat con successo');
	    						$localStorage.questions_threats = response.data;
	    						deferred.resolve(response);
	    					},
	    					function(errResponse) {
	    						console.error('Errore nel caricamento dei Questions Threat');
	    						deferred.reject(errResponse);
	    					}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function getAllQuestions_threats(){
	    			return $localStorage.questions_threats;
	    		}
	    		
	    		
	    		function loadThreats_description(threat_id){
	    			var deferred = $q.defer()
	    			$http.get(urls.SERVICE_API+'get_threat_description/'+threat_id)
	    				.then(
	    					function(response){
	    						console.log('Caricata descrizione threat con successo');
	    						deferred.resolve(response.data);
	    					},
	    					function(errResponse) {
	    						console.error('Errore nel caricamento della descrizione del threat');
	    						deferred.reject;
	    					}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function loadComponent_description(component_id){
	    			var deferred = $q.defer()
	    			$http.get(urls.SERVICE_API+'get_component_description/'+component_id)
	    				.then(
	    					function(response){
	    						console.log('Caricata descrizione del component con successo');
	    						deferred.resolve(response.data);
	    					},
	    					function(errResponse) {
	    						console.error('Errore nel caricamento della descrizione del component');
	    						deferred.reject;
	    					}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function loadQuestion_description(question_id){
	    			var deferred = $q.defer()
	    			$http.get(urls.SERVICE_API+'get_question_description/'+question_id)
	    				.then(
	    					function(response){
	    						console.log('Caricata descrizione della question con successo');
	    						deferred.resolve(response.data);
	    					},
	    					function(errResponse) {
	    						console.error('Errore nel caricamento della descrizione della question');
	    						deferred.reject;
	    					}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function loadQueryDescription(entity){
	    			var deferred = $q.defer()
	    			$http.get(urls.SERVICE_API+'get_query_description/'+entity)
	    				.then(
	    					function(response){
	    						console.log('Caricata descrizione query');
	    						deferred.resolve(response.data);
	    					},
	    					function(errResponse) {
	    						console.error('Errore nel caricamento della query');
	    						deferred.reject;
	    					}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function loadParameters(entity){
	    			var deferred = $q.defer()
	    			$http.get(urls.SERVICE_API+'get_parameters/'+entity)
	    				.then(
	    						function(response){
	    							console.log('Caricati parametri con successo');
	    							deferred.resolve(response.data);
	    						},
	    						function(errResponse) {
	    							console.error('Errore nel caricamento dei parametri');
	    							deferred.reject;
	    						}
	    				);
	    			return deferred.promise;
	    		}
	    		
	    		function loadResponse(entity, number_query, id){
	    			var deferred = $q.defer()
	    			$http.get(urls.SERVICE_API+'get_results/'+entity+"/"+number_query+"/"+id)
	    				.then(
	    						function(response){
	    							console.log('Caricate risposte con successo');
	    							deferred.resolve(response.data);
	    						},
	    						function(errResponse) {
	    							console.error('Errore nel caricamento dei parametri');
	    							deferred.reject;
	    						}
	    				);
	    			return deferred.promise;
	    		}
	    	}
	    ]);