package eu.unicampania.ing.securitycatalogeuapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication(scanBasePackages={"eu.unicampania.ing.securitycatalogeuapp"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
@ComponentScan(basePackages="eu.unicampania.ing.securitycatalogeuapp.controller")
@EntityScan(basePackages="eu.unicampania.ing.securitycatalogeuapp.model")

public class App {
/*
 * Il main viene usato solo come punto di accesso all'applicazione, per convenzione java,
 * questo non fa altro che delegare l'avvio dell'applicativo web alla classe SpringApplication, chiamando run.
 * Questo avvierà il server web Tomcat configurato automaticamente.
 * Viene passata la classe SpringBootCRUDApp perchè rappresenta il componente Spring principale.
 * L'array viene passato nel caso si volessero passare degli argomenti da riga di comando
 */
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
