package eu.unicampania.ing.securitycatalogeuapp.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;

@Entity //In questo modo andiamo a dire ad Hibernate di cheare una tabella da questa classe
@Table(name="threats")
public class Threat implements Serializable{ 
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name = "threat_name", nullable = false)
	private String threat_name;
	
	@Column(name = "threat_description", nullable = false)
	private String threat_description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getThreat_name() {
		return threat_name;
	}

	public void setThreat_name(String threat_name) {
		this.threat_name = threat_name;
	}

	public String getThreat_description() {
		return threat_description;
	}

	public void setThreat_description(String threat_description) {
		this.threat_description = threat_description;
	}
	
	
}
