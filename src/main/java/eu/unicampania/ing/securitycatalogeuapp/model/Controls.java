package eu.unicampania.ing.securitycatalogeuapp.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity //In questo modo andiamo a dire ad Hibernate di creare una tabella da questa classe
@Table(name="controls")
public class Controls implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="control_id", nullable = false)
	private String control_id;
	
	@Column(name = "control_name", nullable = false)
	private String control_name;
	
	@Column(name = "control_description", nullable = false)
	private String control_description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getControl_name() {
		return control_name;
	}

	public void setControl_name(String control_name) {
		this.control_name = control_name;
	}

	public String getControl_description() {
		return control_description;
	}

	public void setControl_description(String control_description) {
		this.control_description = control_description;
	}

	public String getControl_id() {
		return control_id;
	}

	public void setControl_id(String control_id) {
		this.control_id = control_id;
	}

	
	
}
