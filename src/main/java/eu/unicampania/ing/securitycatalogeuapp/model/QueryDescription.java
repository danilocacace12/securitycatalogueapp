package eu.unicampania.ing.securitycatalogeuapp.model;

import java.util.ArrayList;
import java.util.List;

public class QueryDescription {
	//Descrizone query Threat
	public static final  String QueryThreat1Description = "Dato un threat, quali asset vengono minacciati da esso?";
	public static final  String QueryThreat2Description = "Dato un threat, quale contromisura devo adottare per prevenire la minaccia?";
	
	//Descrizone query Countermeasure
	public static final  String QueryCountermeasure1Description = "Data una contromisura, quale threat prevengo?";
	
	//Descrizone query Weakness
	public static final String QueryWeakness1Description = "Data una Weakness, quali assets ne sono soggetti?";
	
	//Descrizone query Asset
	public static final String QueryAsset1Description = "Dato un Asset, quali Threat minacciano il mio asset?";
	public static final String QueryAsset2Description = "Dato un Asset, quali Weakness sono presenti?";

	public static List<String> queryDescription(String entity){
		List<String> query = new ArrayList<String>();
		
		if(entity.equals("Threat")) {
			query.add(QueryThreat1Description);
			query.add(QueryThreat2Description);
		}
		
		if(entity.equals("Countermeasure")) {
			query.add(QueryCountermeasure1Description);
		}
		
		if(entity.equals("Weakness")) {
			query.add(QueryWeakness1Description);
		}
		
		if(entity.equals("Asset")) {
			query.add(QueryAsset1Description);
			query.add(QueryAsset2Description);
		}
		return query;
	}
	
	
}
