package eu.unicampania.ing.securitycatalogeuapp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import eu.unicampania.ing.securitycatalogeuapp.controller.*;
import eu.unicampania.ing.securitycatalogeuapp.model.*;
import eu.unicampania.ing.securitycatalogeuapp.repositories.*;
import eu.unicampania.ing.securitycatalogeuapp.util.*;
/*
 * Notiamo RestController in questa classe, questo ci fa capire che questa ha un ruolo specifico, ovvero quello di
 * gestire le richieste HTTP in entrata
 */
@RestController
@RequestMapping("/SpringBootApp/api")

public class RestApiController {
	
	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	ThreatRepository threatRepository; //Service which will do all data retrieval/manipulation work
	@Autowired
	Questions_threatRepository questions_threatRepository;
	@Autowired
	Component_typesRepository component_typesRepository;
	@Autowired
	ControlsRepository controlsRepository;
	
	/*
	// -------------------Ricevo tutti i threat---------------------------------------------
	@RequestMapping(value = "/threat", method = RequestMethod.GET)
	public ResponseEntity<List<Threat>> listAllThreat() {
		List<Threat> threats = (List<Threat>) threatRepository.findAll();
		if(threats.isEmpty()) {
			return new ResponseEntity<List<Threat>>(HttpStatus.NO_CONTENT);
			// A scelta è possibile restituire anche HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Threat>>(threats, HttpStatus.OK);
	}
	
		// -------------------Ricevo tutti i Question_threat---------------------------------------------
		@RequestMapping(value = "/questions_threat", method = RequestMethod.GET)
		public ResponseEntity<List<Questions_threat>> listAllQuestions_threat() {
			List<Questions_threat> questions_threats = (List<Questions_threat>) questions_threatRepository.findAll();
			if(questions_threats.isEmpty()) {
				return new ResponseEntity<List<Questions_threat>>(HttpStatus.NO_CONTENT);
				// A scelta è possibile restituire anche HttpStatus.NOT_FOUND
			}
			return new ResponseEntity<List<Questions_threat>>(questions_threats, HttpStatus.OK);
		}
		
		// -------------------Ricevo tutti i Component_type---------------------------------------------
		@RequestMapping(value = "/component_types", method = RequestMethod.GET)
		public ResponseEntity<List<Component_types>> listAllComponent_types() {
			List<Component_types> components_types = (List<Component_types>) component_typesRepository.findAll();
			if(components_types.isEmpty()) {
				return new ResponseEntity<List<Component_types>>(HttpStatus.NO_CONTENT);
				// A scelta è possibile restituire anche HttpStatus.NOT_FOUND
			}
			return new ResponseEntity<List<Component_types>>(components_types, HttpStatus.OK);
		}	
	
		// -------------------Cerco la descrizione del threat---------------------------------------------
		@RequestMapping(value = "/get_threat_description/{threat_id}", method = RequestMethod.GET)
		public ResponseEntity<String> getThreatDescription(@PathVariable("threat_id") String threat_id) {
			long id = Long.parseLong(threat_id);
			if(id==0) {
				return new ResponseEntity<String> ("\"Selezionare un elemento valido\"",  HttpStatus.OK);
			}
			String description;
			Threat threat = threatRepository.findOne(id);
			
			if(threat==null){
				logger.error("è stato impossibile recuperare la descrizione del threat");
				return new ResponseEntity<String> (HttpStatus.NO_CONTENT);
			}
			description ="\""+ threat.getThreat_description() +"\"";
			return new ResponseEntity<String> (description,  HttpStatus.OK);
		}
		
		@RequestMapping(value = "/get_component_description/{component_id}", method = RequestMethod.GET)
		public ResponseEntity<String> getComponentDescription(@PathVariable("component_id") String component_id) {
			Long id = Long.parseLong(component_id);
			if(id==0) {
				return new ResponseEntity<String> ("\"Selezionare un elemento valido\"",  HttpStatus.OK);
			}
			String description;
			
			Component_types component = component_typesRepository.findOne(id);
			if(component == null){
				logger.error("è stato impossibile recuperare la descrizione del component");
				return new ResponseEntity<String> (HttpStatus.NO_CONTENT);
			}
			description ="\""+ component.getDescription() +"\"";
			return new ResponseEntity<String> (description,  HttpStatus.OK);
		}
		
		
		@RequestMapping(value = "/get_question_description/{question_id}", method = RequestMethod.GET)
		public ResponseEntity<String> getQuestionDescription(@PathVariable("question_id") String question_id) {
			
			long id = Long.parseLong(question_id);
			if(id==0) {
				return new ResponseEntity<String> ("\"Selezionare un elemento valido\"",  HttpStatus.OK);
			}
			String description;
			Questions_threat question = questions_threatRepository.findOne(id);
			if(question==null){
				logger.error("è stato impossibile recuperare la descrizione della question");
				return new ResponseEntity<String> (HttpStatus.NO_CONTENT);
			}
			description ="\""+ question.getDescription() +"\"";
			return new ResponseEntity<String> (description,  HttpStatus.OK);
		}
		*/
		@RequestMapping(value = "/get_query_description/{entity}", method = RequestMethod.GET)
		public ResponseEntity<List<String>> getQueryDescription(@PathVariable("entity") String entity){
			List<String> query_description = QueryDescription.queryDescription(entity);
			return new ResponseEntity<List<String>> (query_description, HttpStatus.OK);
		}
		
		@RequestMapping(value = "/get_parameters/{entity}", method = RequestMethod.GET)
		public ResponseEntity<List<String>> getParameters(@PathVariable("entity") String entity){
			List<String> parameters=null;
			if(entity.equals("Threat")) {
				parameters = threatRepository.loadThreatName();
				return new ResponseEntity<List<String>> (parameters, HttpStatus.OK);
			}
			if(entity.equals("Weakness")) {
				parameters = questions_threatRepository.loadQuestionDescription();
				return new ResponseEntity<List<String>> (parameters, HttpStatus.OK);
			}
			if(entity.equals("Asset")) {
				parameters = component_typesRepository.loadComponentsName();
				return new ResponseEntity<List<String>> (parameters, HttpStatus.OK);
			}
			if(entity.equals("Countermeasure")) {
				parameters = controlsRepository.loadControlsName();
				return new ResponseEntity<List<String>> (parameters, HttpStatus.OK);
			}
			else{
				return new ResponseEntity<List<String>> (HttpStatus.NO_CONTENT);
			}
		}

		@RequestMapping(value="/get_results/{entity}/{query}/{id}", method = RequestMethod.GET)
		public ResponseEntity<?> getResults(@PathVariable("entity") String entity, @PathVariable("query") String querys, @PathVariable("id") String ids){
			long id = Long.parseLong(ids)+1;
			int query = Integer.parseInt(querys);
			System.out.println("Entity: "+entity+" Numero di query: "+query+" ID: "+id);
			if(entity.equals("Threat")) {
				if(query==0) {
					List<Component_types> response = (List<Component_types>) component_typesRepository.threatQuery1(id);
					response.add(0, creaIndiceComponent_types());
					return new ResponseEntity<List<Component_types>> (response, HttpStatus.OK);
				}
				if(query==1) {
					List<Controls> response = (List<Controls>) controlsRepository.threatQuery2(id);
					response.add(0, creaIndiceControls());
					return new ResponseEntity<List<Controls>> (response, HttpStatus.OK);
				}
			}
			if(entity.equals("Weakness")) {
				if(query==0) {
					List<Component_types> response = (List<Component_types>) component_typesRepository.weaknessQuery1(id);
					response.add(0, creaIndiceComponent_types());
					return new ResponseEntity<List<Component_types>> (response, HttpStatus.OK);
				}
			}
			if(entity.equals("Asset")) {
				if(query==0) {
					List<Threat> response = (List<Threat>) threatRepository.assetQuery1(id);
					response.add(0, creaIndiceThreat());
					return new ResponseEntity<List<Threat>> (response, HttpStatus.OK);
				}
				if(query==1) {
					List<Questions_threat> response = (List<Questions_threat>) questions_threatRepository.assetQuery2(id);
					response.add(0, creaIndiceQuestions_threat());
					return new ResponseEntity<List<Questions_threat>> (response, HttpStatus.OK);
				}
			}
			if(entity.equals("Countermeasure")) {
				if(query==0) {
					List<Threat> response = (List<Threat>) threatRepository.countermeasureQuery1(id);
					response.add(0, creaIndiceThreat());
					return new ResponseEntity<List<Threat>> (response, HttpStatus.OK);
				}
			}
				return new ResponseEntity<List<String>> (HttpStatus.NO_CONTENT);
		}
		
		private Component_types creaIndiceComponent_types() {
			Component_types indice = new Component_types();
			long n = 1;
			indice.setId(n);
			return indice;
		}
		
		private Controls creaIndiceControls() {
			Controls indice = new Controls();
			long n = 2;
			indice.setId(n);
			return indice;
		}
		
		private Questions_threat creaIndiceQuestions_threat() {
			Questions_threat indice = new Questions_threat();
			long n=3;
			indice.setId(n);
			return indice;
		}
		
		private Threat creaIndiceThreat() {
			Threat indice = new Threat();
			long n=4;
			indice.setId(n);
			return indice;
		}
}
