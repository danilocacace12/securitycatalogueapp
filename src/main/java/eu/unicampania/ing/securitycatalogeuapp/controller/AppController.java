package eu.unicampania.ing.securitycatalogeuapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * Notiamo Controller in questa classe, questo ci fa capire che questa ha un ruolo specifico, ovvero quello di
 * gestire le richieste HTTP in entrata
 */
@Controller
public class AppController {

	@RequestMapping("/")
	String home(ModelMap modal) {
		// Modal viene usato per aggiungere attributi che servono per la view, che verranno utilizzati per renderizzarla
		// in questo caso passiamo la stringa Titolo e la salviamo come title, e verrà utilizzata per dare il nome alla nostra scheda
		modal.addAttribute("title","SpringAPP");
		return "index";
	}
	
	//Mappo tutte le altre richieste possibii restituendo l'emento con stesso nome, anche se non ci sono, qundi darà errore
	@RequestMapping("/partials/{page}")
	String partialHandler(@PathVariable("page") final String page) {
		return page;
	}

}
