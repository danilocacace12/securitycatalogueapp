package eu.unicampania.ing.securitycatalogeuapp.repositories;

import java.util.List;
import org.springframework.data.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.unicampania.ing.securitycatalogeuapp.model.*;

/*
 * Questo verrà automaticametne implementato da Spring in un Bean chiamato
 * ThreatRepository, CRUD refers Create,Reade,Update,Delete
 */

@Repository
public interface ControlsRepository extends JpaRepository<Controls, Long>{
	
	@Query(value = "select control_name from controls", nativeQuery = true)
	List<String> loadControlsName();
	
	@Query(value = "select * from controls where id in (select threats_suggested_controls.suggested_controls_id as id from threats_suggested_controls join threats on threats_suggested_controls.threat_id = threats.id where threats.id = :idThreat)", 
			nativeQuery = true)
	List<Controls> threatQuery2(@Param("idThreat") long idThreat);
}
