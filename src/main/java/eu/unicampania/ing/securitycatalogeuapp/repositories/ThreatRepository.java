package eu.unicampania.ing.securitycatalogeuapp.repositories;

import java.util.List;
import org.springframework.data.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.unicampania.ing.securitycatalogeuapp.model.*;

/*
 * Questo verrà automaticametne implementato da Spring in un Bean chiamato
 * ThreatRepository, CRUD refers Create,Reade,Update,Delete
 */

@Repository
public interface ThreatRepository extends JpaRepository<Threat, Long>{

	@Query(value = "SELECT threat_name FROM threats", nativeQuery = true)
	List<String> loadThreatName(); 
	
	@Query(value="select * from threats where id in (select threats_suggested_controls.threat_id as id from threats_suggested_controls join controls on threats_suggested_controls.suggested_controls_id = controls.id where controls.id = :idCountermeasure) ",
			nativeQuery = true)
	List<Threat> countermeasureQuery1(@Param("idCountermeasure") long idCountermeasure);
	
	@Query(value = "select * from threats where threats.id in (select threats_component_types.threat_id as id from threats_component_types join component_types on threats_component_types.component_types_id = component_types.id where component_types.id = :idAsset)",
			nativeQuery = true)
	List<Threat> assetQuery1(@Param("idAsset") long idAsset);
}
