package eu.unicampania.ing.securitycatalogeuapp.repositories;

import java.util.List;
import org.springframework.data.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.unicampania.ing.securitycatalogeuapp.model.*;

/*
 * Questo verrà automaticametne implementato da Spring in un Bean chiamato
 * ThreatRepository, CRUD refers Create,Reade,Update,Delete
 */

@Repository
public interface Questions_threatRepository extends CrudRepository<Questions_threat, Long>{
	@Query(value = "select description from questions_threat", nativeQuery = true)
	List<String> loadQuestionDescription();
	
	@Query(value = "select * from threats_questions join questions_threat on threats_questions.questions_id = questions_threat.id where threats_questions.threat_id in (select threats_component_types.threat_id as threat_id from threats_component_types join component_types on threats_component_types.component_types_id = component_types.id where component_types.id = :idAsset)",
			nativeQuery = true)
	List<Questions_threat> assetQuery2(@Param("idAsset") long idAsset);
}
