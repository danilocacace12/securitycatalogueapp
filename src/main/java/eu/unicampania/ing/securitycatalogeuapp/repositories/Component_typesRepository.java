package eu.unicampania.ing.securitycatalogeuapp.repositories;

import java.util.List;
import org.springframework.data.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.unicampania.ing.securitycatalogeuapp.model.*;

/*
 * Questo verrà automaticametne implementato da Spring in un Bean chiamato
 * ThreatRepository, CRUD refers Create,Reade,Update,Delete
 */

@Repository
public interface Component_typesRepository extends CrudRepository<Component_types, Long>{
	@Query(value = "select name from component_types", nativeQuery = true)
	List<String> loadComponentsName();

	@Query(value = "select * from component_types where id in(select threats_component_types.component_types_id from threats_component_types join threats on threats_component_types.threat_id = threats.id where threats.id = :idThreat)", 
			nativeQuery = true)
	List<Component_types> threatQuery1(@Param("idThreat") long idThreat);
	
	@Query(value = "select * from component_types where id in (select threats_component_types.component_types_id as id from threats_component_types join threats on threats_component_types.threat_id = threats.id where threats.id in (select threats_questions.threat_id as id from threats_questions join questions_threat on threats_questions.questions_id = questions_threat.id where questions_threat.id = :idQuestionsThreat))",
			nativeQuery = true)
	List<Component_types> weaknessQuery1(@Param("idQuestionsThreat") long idQuestionsThreat); 
}
